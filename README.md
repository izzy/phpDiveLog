<!-- Name: phpDiveLog -->
<!-- Version: 8 -->
<!-- Last-Modified: 2019/12/15 20:42:08 -->
<!-- Author: izzy -->

### What is phpDiveLog?
*phpDiveLog* displays the information of your Palms [AquaDiveLog](http://www.aquadivelog.org/) logbook based on CSV files you generate with the Java Conduit shipped with AquaDiveLog – or your dive data from [Subsurface](https://subsurface-divelog.org/), converted to CSV with the help of scripts included with *phpDiveLog.* Providing some "magics" it allows you to combine these data with additional information, such as pictures or other external data sources.

As for now, phpDiveLog offers you no features to edit these data. But it provides you with facilities to display...

* ...the Dive Logbook. Here you browse through the list of your dives (in the config file, you may specify how many entries should be displayed per page). For detailed information, a click on the dive# brings you directly to the log books entry.
* ...the Dive Statistics, which show you some basic stats about your dives, such as max/avg depth and divetime etc.
* ...the Dive Sites Information, which provides you with a list of your dive sites. Again, a click on the site# brings you to the details page
* ...all Dive Sites in [Google Maps](https://maps.google.com/)/OpenStreetMap (via [OSMTools](https://osm.quelltextlich.at/)) or [Google Earth](https://earth.google.com/) (via KML Download) with a single click
* ...the Divers Information, where you can define some "about me" items

Additionally, you also can export your dive and site information to PDF - in order to maintain a "paper version" of your logbook.

### What are the requirements to use phpDiveLog?
* For data conversion from AquaDiveLog (see `install/adl/`) you need to have a Java interpreter installed (refer to the AquaDiveLog documentation for details).
* For data conversion from Subsurface (see `install/subsurface/`) you'll need a PHP CLI interpreter.

phpDiveLog itself only requires a running web server with PHP (version 7 or higher recommended) support.

### What license is used?
*phpDiveLog* uses the [Gnu Public License v2](https://codeberg.org/izzy/phpDiveLog/blob/master/install/LICENSE).

### Where can I find more information?
* in the [Demo](https://www.izzysoft.de/software/demos/phpdivelog/)
* in the [Wiki](https://codeberg.org/izzy/phpDiveLog/wiki)
* within the downloaded files
* on the [AquaDiveLog Homepage](http://www.aquadivelog.org/)
