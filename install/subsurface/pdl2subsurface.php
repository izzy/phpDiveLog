#!/usr/bin/php
<?php
/*
  convert from PDL CSV to Subsurface importable CSV
  This is just a "Quick-and-Dirty" approach I share with you.
  Worked for me, but might kill your cat.
*/

// ========================================================[ Configuration ]===
// setup your locations here:
// path to the phpDiveLog include dir:
$inc = "/path/to/divelog/inc";
// path to where your PDL CSV files reside
$datadir = "/path/to/divelog/diver/user/data";
// where the generated CSV files (for Subsurface to import) shall be placed
$outdir = './csv';

$inc = "/web/rehbergs.info/izzy/diving/divelog/inc";
$datadir = "/web/rehbergs.info/izzy/diving/divelog/diver/izzy/data";
//$datadir = "/web/rehbergs.info/izzy/diving/divelog/diver/izzy/notes";
//$inc = '/foobar';
//$datadir = '/foobar';

// =====================================[ Initial checks: all ready to go? ]===
if ( $inc == "/path/to/divelog/inc" || $datadir == "/path/to/divelog/diver/user/data" ) {
  echo "\nyou first must configure \$inc and \$datadir at the top of the script.\n\n";
  exit;
}
if ( !file_exists($inc) || !is_dir($inc) ) {
  echo "\nThe phpDiveLog include dir specified does not exist:\n'$inc'\n\n";
  exit;
}
if ( !file_exists("${inc}/class.csv.inc") ) {
  echo "\nThe phpDiveLog include dir specified does not contain the PDL classes:\n'$inc'\n\n";
  exit;
}
if ( !file_exists("${datadir}/logbook.csv") ) {
  echo "\nCould not find your logbook.csv in the \$datadir specified:\n'$datadir'\n\n";
  exit;
}
if ( !file_exists($outdir) || !is_dir($outdir) || !is_writable($outdir) ) {
  echo "\nThe output directory specified does not exist, is no directory or\ncannot be written to:\n'$outdir'\n\n";
  exit;
}


// ==============================================================[ Helpers ]===
require_once("${inc}/class.csv.inc");

/** Convert coordinates
 * (from class.import.php)
 * @function mk_coord
 * @param string str (Coords as from ADL)
 * @return decimal coord
 */
function mk_coord($str) {
 if ( preg_match("/([0-9]+)[^0-9]+([0-9]+)[^0-9.]+([0-9.]+)[^0-9]+/",$str,$match) ) {
   $code[0] = $match[1]; $code[1] = $match[2]; $code[2] = $match[3];
 } elseif ( preg_match("/([0-9]+)[^0-9]+([0-9.]+)[^0-9.]+/",$str,$match) ) {
   $code[0] = $match[1]; $code[1] = floor($match[2]);
   $code[2] = ($match[2] - floor($match[2])) * 60;
 } elseif ( preg_match("/([0-9.]+)[^0-9.]+/",$str,$match) ) {
   $code[0] = floor($match[1]);
   $code[1] = floor( ($match[1] - $code[0]) * 60);
   $code[2] = floor( (($match[1] - $code[0]) * 60 - $code[1]) * 60 );
 }
 if ( substr($str,strlen($str)-1)=="N" || substr($str,strlen($str)-1)=="E" ) {
   $code[4] = 1;
 } else {
   $code[4] = -1;
 }
 $min  = $code[1] + $code[2]/60;
 $coord = ($code[0] + $min/60) * $code[4];
 return $coord;
}

/** Format date strings to 'YYYY-MM-DD'
 * @function mk_date
 * @param string str    date in 'DD MON YYYY' format (PDL)
 * @return string str   date in 'YYYY-MM-DD' format (Subsurface)
 */
function mk_date($str) {
  static $m = ['','January','February','March','April','May','June','July','August','September','October','November','December'];
  preg_match('!^\s*(\d+)\s+(\w+)\s+(\d+)\s*$!',$str,$match);
  foreach ($m as $key=>$val) if ($match[2]==$val) { $mon = $key; break; }
  if ($mon<10) $mon = "0${mon}";
  if ($match[1]<10) $match[1] = "0".$match[1];
  return $match[3].'-'.$mon.'-'.$match[1];
}
/** adjust air format for PDL: strip "NTX" if exists, else return empty string
 * @function mk_o2
 * @param string str    air from PDL (e.g. 'air' or 'NTX36')
 * @return string str   air in Subsurface format (e.g. '' or '36')
 */
function mk_o2($str) {
  if ( preg_match('!NTX(\d+)!i',$str,$match) ) return $match[1];
  return '';
}


// =========================================================[ Main: Do it! ]===
$divelog = new csv(";",'"',TRUE);
$divelog->import($datadir . '/logbook.csv');
$divelog->sort('dive#');
$divesites = new csv(";",'"',TRUE);
$divesites->import($datadir . '/divesites.csv');
$divesites->sort('id');

// build array of divesites by site ID
$sites = [];
foreach ($divesites->data as $site) $sites[$site['id']] = $site;

// walk divelog, adjust columns, connect site data and port dive profiles (if exist)
$records = [];
$precs = [];
foreach ($divelog->data as $rec) {
  $dsite = $sites[$rec['site_id']];
  if ( empty($dsite['latitude']) ) $gps = '';
  else $gps = mk_coord($dsite['latitude']) . ' ' . mk_coord($dsite['longitude']);
  $records[] = [
    'dive#'     => $rec['dive#'],
    'date'      => mk_date($rec['date']),
    'time'      => trim($rec['time']),
    'location'  => trim($rec['location']) . ': ' . trim($rec['place']),
    'gps'       => $gps,
    'max_depth' => preg_replace('!.*?([\d\.]+).*!','$1',$rec['depth']),
    'duration'  => preg_replace('!.*?(\d+).*!','$1',$rec['divetime']).':00',
    'cyl_size'  => trim($rec['tank_volume']),
    'o2'        => mk_o2($rec['tank_gas']),
    'start_pressure' => trim($rec['tank_in']),
    'end_pressure'   => trim($rec['tank_out']),
    'buddy'     => trim($rec['buddy']),
    'watertemp' => trim($rec['watertemp']),
    'airtemp'   => trim($rec['airtemp']),
    'suit'      => trim($rec['suittype']) . ' ' . trim($rec['suitname']), // ommitting suitweight
    'weight'    => trim($rec['weight']),
    'rating'    => trim($rec['rating']),
    'notes'     => trim($rec['notes'])      // might need to cleanup/convert PDL specific formattings like [url]
  ];
  // profiles, if exist:
  $proffile = 'dive'.sprintf("%'.05d",$rec['dive#']).'_profile.csv';
  if ( file_exists($datadir.'/'.$proffile) ) {
    echo "$proffile\n";
    $prof = new csv(";",'"',TRUE);
    $prof->import($datadir.'/'.$proffile);
    $prec = [];
    foreach($prof->data as $prow) {
      $prec[] = [
        'dive#'     => $rec['dive#'],
        'date'      => mk_date($rec['date']),
        'time'      => trim($rec['time']),
        'sample time' => $prow['time'],
        'sample depth'=> $prow['depth'],
        'o2'        => mk_o2($prow['gas']),
        'warning'   => $prow['warning']
      ];
    }
    $fp = fopen($outdir.'/'.$proffile,'w');
    fputcsv($fp, ['dive#','date','time','sample time','sample depth','o2','sample setpoint'],';');
    foreach ($prec as $row) fputcsv($fp, $row,';');
    fclose($fp);
  }
}

// Now create the CSV for Subsurface
$fp = fopen($outdir.'/divelog.csv','w');
fputcsv($fp, [
  'dive#','date','time','location','gps','max. depth','duration','cyl. size','o2','start pressure','end pressure',
  'buddy','watertemp','airtemp','suit','weight','rating','notes'
],';');
foreach ($records as $rec) fputcsv($fp, $rec,';');
fclose($fp);

// $csv->data: array 0..n of dive/site-records
$dc = count($divelog->data); $sc = count($divesites->data);
echo "$dc dives, $sc sites.\n";
print_r($records);
//print_r($sites);
//print_r($divelog->data);
exit;


?>